# Instrumenting Golang apps with Opentelemtry SDK and Gitlab

This step-by-step guide is meant to show you how to instrument your Golang application using OpenTelementry SDK and explore them using Gitlab UI.

The Otel Metrics and Traces support in Gitlab is a feature we have been working on for some time.
They are both in the Beta stage at the moment.

We will be only adding here tracing and metrics instrumentation, as support for logs in Opentelemetry SDK for Golang is still WiP and support for Opentelemtry Logs in Gitlab is in the Experimental phase.

## Prerequisites
A prerequisite for this HowTo is a Gitlab project with following feature flags enabled:
* observability_group_tab
* observability_tracing
* observability_metrics

On top of that, you will need an Internet connection and a working Golang build chain not older than 1.21.4.
Earlier versions may work but we haven't tested them - use at your own risk.
Only Linux and Macs are supported.

## Quickstart

If you would like to immediately get going, you could just check out the `main` branch and:
* obtain Gitlab Access Token for the project where you intend to send traces, with scopes `read_api`, `read_observability`, `write_observability` and store it in `.token` file in main directory of the repository.
* determine the ingestion URL. Its format follows `https://<observability_backend_url>/v3/<top-level-group_id>/<project_id>/ingest/`
  * `observability_backend_url` is the address of the GitLab observability backend. For SaaS, it is `observe.gitlab.com`
  * `top-level-group_id` is the ID of the top-most group to which our project belongs. If you are using Gitlab SaaS and your project is nested under `gitlab-org`, this is going to be `9970`.
  * `project_id` is the ID of the project where you will be viewing traces and metrics.

Once you have all above, in the first terminal window launch:

```bash
make OTEL_ENDPOINT=https://observe.gitlab.com/v3/9970/<project_id>/ingest/ run-fibber
```

and in the second terminal window:

```bash
make OTEL_ENDPOINT=https://observe.gitlab.com/v3/9970/<project_id>/ingest/ run-traefik
```

The final step is to issue at least two curl requests to fetch some data from the API:

```bash
$ curl http://localhost:9080/v1/api/fibonacci\?n\=100 -v
*   Trying 127.0.0.1:9080...
* Connected to localhost (127.0.0.1) port 9080 (#0)
> GET /v1/api/fibonacci?n=100 HTTP/1.1
> Host: localhost:9080
> User-Agent: curl/7.81.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Length: 21
< Content-Type: text/plain; charset=utf-8
< Date: Thu, 07 Mar 2024 20:49:46 GMT
< 
* Connection #0 to host localhost left intact
354224848179261915075
```

You can now explore metrics na traces in the UI.

## Sample application stack
The Go application that we will be instrumenting is a simple HTTP API server that we called "Fibber" in front of which stands Traefik reverse proxy.

![block_diagram](./images/block_diagram.drawio.png)

### Fibber
Fibber uses [go-gin](https://gin-gonic.com/) HTTP router and exposes a single endpoint that calculates the n-th Fibonacci number.

It has a simple command line help:
```bash
$ go run ./cmd/fib/main.go -help
Usage of /tmp/go-build982697996/b001/exe/main:
  -metrics-bind-address string
        The address the metric endpoint binds to. (default ":8081")
  -service-bind-address string
        The address the service endpoint binds to. (default ":8080")
```

The first 93 indexes are calculated using the plain `int64` type.
Above that, we use `math/big.Int`.
To get more complex traces, we do not cache intermediate indexes, only the final ones.

### Traefik
Traefik runs a minimal configuration, with the dashboard available on `http://localhost:9079` and the main entry point being `http://localhost:9080` which then proxies all requests to fibber listening on port `http://localhost:8080`.
We are running Traefik v3, RC1 as it provides us with native otel instrumentation (both traces and metrics).
We expect to switch our infra to Traefik v3 as soon as it goes GA.

## Tracing and metrics support in Gitlab

The metrics and traces that we will collect in our application will be sent to Gitlab where they are stored and can later be viewed using the UI.
Once we send the traces/metrics to Gitlab, we will be able to access them in the `Monitor` section of the left-pane menu under `Traces` and `Metrics` entries:

![traces_metrics_menu_location](./images/traces_metrics_menu_location.png)

## Structure of this repository

### `bare` and `main` branches

In this repository, there are two versions of this application - instrumented and un-instrumented.
The instrumented one can be found on the `main` branch, whereas the un-instrumented one is in the `bare` branch in this repo.
You can compare the codebases to see how OTEL instrumentation changes code and to check the code you come up with with the reference once while you work through this HowTo.

### Running Traefik and Fibber

To streamline installation and running Traefik and Fibber, a Makefile was provided.
You need to open two console windows, one for Fibber and one for Traefik.

In the first terminal window launch:

```bash
make run-fibber
```

and in the second terminal window:

```bash
make run-Traefik
```

You can then issue a curl request to fetch some data from the API:

```bash
$ curl http://localhost:9080/v1/api/fibonacci\?n\=100 -v
*   Trying 127.0.0.1:9080...
* Connected to localhost (127.0.0.1) port 9080 (#0)
> GET /v1/api/fibonacci?n=100 HTTP/1.1
> Host: localhost:9080
> User-Agent: curl/7.81.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Length: 21
< Content-Type: text/plain; charset=utf-8
< Date: Thu, 07 Mar 2024 20:49:46 GMT
<
* Connection #0 to host localhost left intact
354224848179261915075
```

If you can see this response, then you are ready to start!

# Instrumenting Traefik

## Traces

We will start the instrumentation process with Traefik as it is much more straightforward.
Traefik configuration can be specified either on the command line or via config files (TOML, YAML).
In this HowTo, we will be using command line switches only.
We will be adding them to the Makefile target that starts Traefik.

We start by enabling tracing - add to the Traefik command line the following option:

```
--tracing.otlp=true
```

Gitlab observability backend currently only supports OTEL HTTP endpoint, GRPC is not supported.
The URL that we will be using has a format:

```
https://<observability_backend_url>/v3/<top-level-group_id>/<project_id>/ingest/v1/traces
```

* `observability_backend_url-id` is the address of the GitLab observability backend. For SaaS, it is `observe.gitlab.com`
* `top-level-group_id` is the ID of the top-most group to which our project belongs.
If you are using Gitlab SaaS and your project is nested under `gitlab-org`, this is going to be `9970`.
* `project_id` is the ID of the project where you will be viewing traces and metrics.

This gives us:

```
--tracing.otlp.http=true
--tracing.otlp.http.endpoint=https://observe.gitlab.com/v3/9970/<project_id>/v1/traces
```

The OTEL ingestion endpoint above uses authn and authz.
We need a Gitlab Access Token at the `Developer` level with scopes:
* `read_observability`
* `write_observability`
* `read_api`

```
--tracing.otlp.http.headers.private-token=glat-....
```

Let's issue a few curl requests and see if we got any traces:

```
$ curl http://localhost:9080/v1/api/fibonacci\?n\=100 -v
```

![traces_traces](./images/traces_traces.png)

Traefik started sending tracing headers to upstream servers thanks to the instrumentation we have enabled (the `TraceParent` header).
Context propagation via HTTP headers is one of the core concepts of distributed tracing - spans from different applications are correlated using these headers.

```
E.....@.@.cn............cc1U.}.DP.......GET /v1/api/fibonacci?n=100000 HTTP/1.1
Host: localhost:9080
User-Agent: curl/7.81.0
Accept: */*
Traceparent: 00-ada06243f3df282f129a3106f4b24503-651f4ce13d4ea380-01
X-Forwarded-For: 127.0.0.1
X-Forwarded-Host: localhost:9080
X-Forwarded-Port: 9080
X-Forwarded-Proto: http
X-Forwarded-Server: zara.engel.vespian.net
X-Real-Ip: 127.0.0.1
Accept-Encoding: gzip


21:00:12.638976 IP 127.0.0.1.8080 > 127.0.0.1.38060: Flags [.], ack 374, win 509, length 0
E..(2.@.@.
2.............}.Dcc2.P.......
21:00:12.639162 IP 127.0.0.1.8080 > 127.0.0.1.38060: Flags [P.], seq 1:4097, ack 374, win 512, length 4096: HTTP: HTTP/1.1 200 OK
E..(2.@.@..0.............}.Dcc2.P.......HTTP/1.1 200 OK
Content-Type: text/plain; charset=utf-8
Date: Thu, 07 Mar 2024 20:00:12 GMT
Transfer-Encoding: chunked

51a3
2597406934722172416
```

## Metrics

Adding metrics instrumentation to Traefik follows the same pattern:

Enable OTLP metrics support:

```
--metrics.otlp=true \
```

Configure Traefik to add some additional labels to the generated metrics:

```
--metrics.otlp.addEntryPointsLabels=true \
--metrics.otlp.addRoutersLabels=true \
--metrics.otlp.addServicesLabels=true \
```

OTLP metrics use a push model, contrary to the pull model used by Prometheus.
This means that instead of having a process/pod that periodically pulls metrics from Traefik, Traefik periodically sends metrics to Gitlab on its own.

```
--metrics.otlp.pushInterval=10s \
```

And finally - the HTTP endpoint configuration.
The same as for Tracing, GRPC is not supported by GitLab Observability Backend.

```
--metrics.otlp.http=true
--metrics.otlp.http.endpoint=https://observe.gitlab.com/v3/9970/<project_id>/v1/metrics
--metrics.otlp.http.headers.private-token=glat-...
```

We should be now able to see Traefik metrics in the GitLab UI:

![traces_metrics](./images/traces_metrics.png)

# Instrumenting Fibber

For this workshop, a simple package was provided that does most of OTEL SDK initialization and autointruments Gin router for us.
It can be found in `pkg/instrumentation` folder in this repo.
Writing it from scratch is outside of the scope of this Howto and is considered an advanced usage.
We are working on providing abstractions, probably as part of Labkit, to provide a similar experience when instrumenting the Gitlab code base ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2725).
We encourage you to take a look and see how the Otel SDK is set up under the hood!

## Traces

We start with traces again.

Let's make Fibber accept Otel Endpoint and Access Token as command line options.
Add the following command line flags to the `parseCmdline()` function:

```
+       flag.StringVar(&otlpEndpoint,
+               "otlp-endpoint", "", "otlp collector endpoint, without `/1/traces` or `/v1/metrics` suffix")
+       flag.StringVar(&otlpTokenSecretFile,
+               "otlp-token-file", "", "file that contains Access Token to use when connecting to otel collector")
```

The instrumentation library expects the token to be stored as a file, hence let's write it to the `.token` file in the repo's main directory and we will pass its path on the command line.

To see logs output from the Otel library, we need to work around an issue with Uber Zap, due to a mismatch between log levels that Zap is using and Logger is using:

```diff
	loggerCfg := zap.Config{
-               Level:             zap.NewAtomicLevelAt(zapcore.DebugLevel),
+               Level:             zap.NewAtomicLevelAt(zapcore.Level(-6)),
```

Our next step is constructing OTEL resource.
This is an object that is shared by all Otel signals and is used to identify the entities sending the data via attributes (e.g. service Name) as well as the version of the semantic convention schema.

```diff
+       // Construct otel resource:
+       resource, err := instrumentation.ConstructOTELResource(ctx, "fibber", "1.0.0")
+       if err != nil {
+               logger.Errorw("defining otel resource failed", zap.Error(err))
+               return 1
+       }
+
```

And finally, we set up tracing itself:

```diff
+       tracerProvider, tracePropagator, traceProviderDoneF, err := instrumentation.ConstructOTELTracingTools(
+               ctx,
+               logger,
+               resource,
+               otlpEndpoint, "", otlpTokenSecretFile,
+       )
+       if err != nil {
+               logger.Errorw("constructing otel trace provider failed", zap.Error(err))
+               return 1
+       }
+       defer func() {
+               err := traceProviderDoneF()
+               if err != nil {
+                       logger.Errorw("shutting down trace provider failed", zap.Error(err))
+               }
+       }()
```

Trace provider can be thought of as a factory of `Span` objects.
One of the more Spans linked together in an acyclic tree constitutes a Trace.
Trace is a representation of a code path that the request traversed while it was being handled.

TracePropagator is an object that is responsible for the distributed nature of tracing.
As you already saw in the Traefik section, the ID of the parent span is propagated in the HTTP request header.
There are different standards for how this information can be codified, and the task of the tracePropagator is to handle them and properly extract information included by entities earlier in the processing chain.
This information is then used to create a new span that will be a parent to all
spans created by this entity.

We have set up tracing, and our application will be able to create new spans.
There is a middleware available for Gin - `go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin`.
It automatically starts a new span for each incoming request which we will then be able to access and create new child spans from.

```
+       // Route traces:
+       routerMain.Use(
+               otelgin.Middleware(
+                       "fibdemo",
+                       otelgin.WithPropagators(tracePropagator),
+                       otelgin.WithTracerProvider(tracerProvider),
+               ),
+       )
```

It is crucial to enable context fallback, as otherwise, Otel SDK will silently create NOOP spans, as it will not be able to access spans created by otelgin middleware.

```
	routerMain.ContextWithFallback = true
```

The last step is adding spans to the functions you think should be creating one.
We will be re-using code from the `instrumentation` package that was provided to you in this repo:

```
+       var span trace.Span
+       ctx, span = instrumentation.NewSubSpan(ctx, "fibber.lookupCache", trace.SpanKindClient)
+       defer span.End()
```

There is more to span than just racing execution path (e.g. span events, exemplars, span attributes, etc...) but this is outside of the scope of this exercise.

Issue a few requests to your Fibber app through Traefik.
In GitLab UI you should have two kinds of traces:

* value was calculated:

![calculated fibonacci](./images/calculated_fibonacci.png)

* value was fetched from the cache:

![cached fibonacci](./images/cached_fibonacci.png)


# Metrics

Instrumenting Metrics is a bit trickier as we need to provide backward compatibility with Prometheus metrics and provide an incremental migration path.
Metrics instrumentation will also use the otel resource that we created in the Traces section.

We start with creating a Meter provider.
Just like the traces provider, the meter provider is a factory that we later use to create individual counters, histograms, gauges, etc. (Meters in SDK terminology.

We are passing the Prometheus registry here, because this way SDK will make Prometheus metrics available to Otel SDK and vice versa - Otel Metrics will be available in the Prometheus endpoint.
```
+       meterProvider, meterProviderDoneF, err := instrumentation.ConstructOTELMeterProvider(
+               ctx,
+               logger,
+               resource,
+               metricsRegistry,
+               otlpEndpoint, otlpCACertificate, otlpTokenSecretFile,
+       )
+       if err != nil {
+               logger.Errorw("constructing otel meter provider failed", zap.Error(err))
+               return 1
+       }
+       defer func() {
+               err := meterProviderDoneF()
+               if err != nil {
+                       logger.Errorw("shutting down meter provider failed", zap.Error(err))
+               }
+       }()
```

We can have multiple Meter providers.
Here we create a default one, which will add to all metrics labels that specify the name of our application and version of instrumenation.

```
+       defaultMeter := instrumentation.NewDefaultMeter(meterProvider)
```

We use the default meter in two different ways.
One is instrumenting all the incoming requests generically by creating a Gin middleware:

```
-       routerMain.Use(instrumentation.RouteMetricsPrometheus(metricsRegistry))
+       routerMain.Use(instrumentation.RouteMetricsOtel(defaultMeter))
```

The other is passing the default meter provider to the constructor of the main fibber object.
We then create an application-specific meter :

```
-       fibr := fibber.New(metricsRegistry)
+       fibr := fibber.New(defaultMeter)
```

```
 type FibberT struct {
        cache map[int]string

-       lookupCount *prometheus.CounterVec
+       lookupCount metric.Int64Counter
 }

-func New(registry prometheus.Registerer) *FibberT {
+func New(m metric.Meter) *FibberT {
        res := &FibberT{
                // Limit is arbitrary
                cache: make(map[int]string),
        }

-       res.lookupCount = prometheus.NewCounterVec(
-               prometheus.CounterOpts{
-                       Name: "lookups_total",
-                       Help: "number of fibonacci lookups",
-               },
-               []string{"from_cache"},
+       var err error
+       res.lookupCount, err = m.Int64Counter(
+               "lookups_total",
+               metric.WithDescription("number of fibonacci lookups"),
        )
-       registry.MustRegister(res.lookupCount)
-
+       if err != nil {
+               // We want to mimic the behavior of prometheus'es MustRegister() call:
+               panic(fmt.Sprintf("lookup counter init: %v", err))
+       }
        return res
 }
```

Finally, we increment a counter every time there is a request to calculate the Fibonacci number:

```
-               fb.lookupCount.WithLabelValues(fmt.Sprintf("%t", from_cache)).Inc()
+               fb.lookupCount.Add(
+                       ctx,
+                       1,
+                       metric.WithAttributes(
+                               attribute.Bool("from_cache", from_cache),
+                       ),
+               )
```

After doing a few requests, metrics should be visible in the UI:

![lookups metric](./images/lookups_metric.png)

# Foreword
This HowTO only scratches the surface of what can be done with Opentelemetry.
As a next step, you could explore how Spans can be enriched with additional information (span events, exemplars, attribututes, etc...) as well as take a closer look into the `instrumentation` package that is part of this repository.

