# FibDemo

This project is meant to help to learn how to instrument your application using OTEL SKD.
It has two relevant branches: `main` and `bare`.
The `main` branch contains the application with all the instrumentation set up.
The `bare` branch contains the application without any instrumentation.

The application itself is a [go-gin](https://gin-gonic.com/)-based HTTP API server that exposes a single endponit, which calculates n-th fibonacci number.
In front of the server sits [traefik](https://traefik.io/) web proxy which is also being instrumented, via config options.

# Running application

## `bare` branch

Running `bare` FibDemo does not produce any traces nor metrics we could observe, but does not require any special configuration either.

In the first terminal window launch:

```bash
make run-fibber
```

and in the second terminal window:

```bash
make run-traefik
```

You can then issue a curl request to fetch some data from the API:

```bash
$ curl http://localhost:9080/v1/api/fibonacci\?n\=100 -v
*   Trying 127.0.0.1:9080...
* Connected to localhost (127.0.0.1) port 9080 (#0)
> GET /v1/api/fibonacci?n=100 HTTP/1.1
> Host: localhost:9080
> User-Agent: curl/7.81.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Length: 21
< Content-Type: text/plain; charset=utf-8
< Date: Thu, 07 Mar 2024 20:49:46 GMT
< 
* Connection #0 to host localhost left intact
354224848179261915075
```

## `main` branch

Running `main` FibDemo requires some additional steps, namelly:
* obtain Gitlab Access Token for the project where you intend to send traces, with scopes `read_api`, `read_observability`, `write_observability` and store it in `.token` file in main directory of the repository.
* determine the ingestion URL.
  Its format follows `https://<instance_url>/v3/<top-level-group-id>/<project-id>/ingest/`
* in case when your instance uses self-signed certificate, save it to `ca.crt` file in the main directory of the repository.

If you are using devvm, you can obtain CA cert by:

```sh
$ kubectl get secret -n cert-manager self-signed-ca-secret -o jsonpath='{ .data.ca\.crt }' | base64 -d > ca.crt
```

Once you have all above, in the first terminal window launch:

```bash
make OTEL_ENDPOINT=https://gob.devvm/v3/22/1/ingest/ run-fibber
```

and in the second terminal window:

```bash
make OTEL_ENDPOINT=https://gob.devvm/v3/22/1/ingest/ run-traefik
```

You can now issue a curl request to fetch some data from the API, just like for `bare` variant.

# Fibber

The application is [go-gin](https://gin-gonic.com/)-based HTTP API server that exposes a single endponit.
It has a simple command line help:

```bash
$ go run ./cmd/fib/main.go -help
Usage of /tmp/go-build982697996/b001/exe/main:
  -metrics-bind-address string
        The address the metric endpoint binds to. (default ":8081")
  -otlp-ca-cert string
        otlp CA certificate to use while connecting over HTTPs
  -otlp-endpoint /1/traces
        otlp collector endpoint, without /1/traces or `/v1/metrics` suffix
  -otlp-token-file string
        file that contains Access Token to use when connecting to otel collector
  -service-bind-address string
        The address the service endpoint binds to. (default ":8080")
```

It calculates Fibonacci numbers of any index.
First 93 indexes are calculated using plain `int64` type.
Above that, we use `math/big.Int`.
Results are cached, but for the purpose of the workshop we do not cache intermediate indexes, only the final one.

Coupled with tracing in Traefik and context propagation, we get two kind of traces:

* value was calculated:

![calculated fibonacci](./images/calculated_fibonacci.png)

* value was fetched from the cache:

![cached fibonacci](./images/cached_fibonacci.png)

When it comes to metrics, it exposes two kinds of metrics for the purpose of demonstrating Otel SDK instrumentation:
* route metrics - implemented as go-gin middleware, gather basic information about the request, request duration, status, etc...
* custom metric `lookups_total` - count the calls to the endpoint and how effective is the cache.

Below is a sample illustration how this metric looks like in our UI:

![lookups metric](./images/lookups_metric.png)


# Traefik

Traefik runs a minimal configuration, with the dashoard available on `http://localhost:9079` and the main entrypoint being `http://localhost:9080` which then proxies all requests to fibber listening on port `http://localhost:8080`.
We are running Traefik v3, RC1 as it provides us with native otel instrumetnation (both traces and metrics).
We expect to switch our infra to Traefik v3 as soon as it goes GA.

When running in `main` branch, Traefik propagates the tracing context to fibber.
Below is a fragment of a tcpdump session, note the `Traceparent` header.

```
E.....@.@.cn............cc1U.}.DP.......GET /v1/api/fibonacci?n=100000 HTTP/1.1
Host: localhost:9080
User-Agent: curl/7.81.0
Accept: */*
Traceparent: 00-ada06243f3df282f129a3106f4b24503-651f4ce13d4ea380-01
X-Forwarded-For: 127.0.0.1
X-Forwarded-Host: localhost:9080
X-Forwarded-Port: 9080
X-Forwarded-Proto: http
X-Forwarded-Server: zara.engel.vespian.net
X-Real-Ip: 127.0.0.1
Accept-Encoding: gzip


21:00:12.638976 IP 127.0.0.1.8080 > 127.0.0.1.38060: Flags [.], ack 374, win 509, length 0
E..(2.@.@.
2.............}.Dcc2.P.......
21:00:12.639162 IP 127.0.0.1.8080 > 127.0.0.1.38060: Flags [P.], seq 1:4097, ack 374, win 512, length 4096: HTTP: HTTP/1.1 200 OK
E..(2.@.@..0.............}.Dcc2.P.......HTTP/1.1 200 OK
Content-Type: text/plain; charset=utf-8
Date: Thu, 07 Mar 2024 20:00:12 GMT
Transfer-Encoding: chunked

51a3
2597406934722172416
```
